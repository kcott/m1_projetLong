package com.example.kevin.testmusic;

import android.app.Activity;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;


public class MainActivity extends Activity {
    MediaPlayer mpNight;
    MediaPlayer mpMP5;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onResume(){
        super.onResume();

        mpNight = MediaPlayer.create(getApplicationContext(),R.raw.night_time_storm);
        mpMP5 = MediaPlayer.create(getApplicationContext(),R.raw.mp5_smg);

        mpNight.setLooping(true);
        mpMP5.setLooping(true);

        mpNight.start();
        mpMP5.start();
    }

    @Override
    protected void onPause(){
        super.onPause();
        mpNight.stop();
        mpMP5.stop();
    }
}
