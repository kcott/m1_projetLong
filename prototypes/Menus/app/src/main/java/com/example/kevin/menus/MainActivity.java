package com.example.kevin.menus;

import android.app.Activity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Layout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;


public class MainActivity extends Activity {
    Button bContinu;
    Button bNewGame;
    Button bMultiplayer;
    Button bSettings;
    Button bExit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Gets buttons to add events
        bContinu    = (Button) findViewById(R.id.bContinu);
        bNewGame    = (Button) findViewById(R.id.bNewGame);
        bMultiplayer= (Button) findViewById(R.id.bMultiplayer);
        bSettings   = (Button) findViewById(R.id.bSettings);
        bExit       = (Button) findViewById(R.id.bExit);

        defineEvents();
    }
    private void defineEvents(){
        bExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
