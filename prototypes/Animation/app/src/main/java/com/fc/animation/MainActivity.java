package com.fc.animation;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;


public class MainActivity extends ActionBarActivity {

    private Animation anima ;
    private View.OnClickListener myclick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch(v.getId()){
                case R.id.b_down : anima.setDir(Animation.DIR_D); break;
                case R.id.b_up : anima.setDir(Animation.DIR_U); break;
                case R.id.b_left : anima.setDir(Animation.DIR_L); break;
                case R.id.b_right : anima.setDir(Animation.DIR_R);break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        anima = new Animation(this);
        setContentView(anima);
        /*setContentView(R.layout.activity_main);
        this.findViewById(R.id.b_down).setOnClickListener(myclick);
        this.findViewById(R.id.b_left).setOnClickListener(myclick);
        this.findViewById(R.id.b_right).setOnClickListener(myclick);
        this.findViewById(R.id.b_up).setOnClickListener(myclick);*/
    }

    @Override
    protected void onPause(){
        super.onPause();
        anima.interrupt();
    }

    @Override
    protected void onResume(){
        super.onResume();
        anima.start();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
