package com.fc.animation;

import android.content.*;
import android.graphics.*;
import android.util.Log;
import android.view.*;


/**
 * Created by fc on 24/03/15.
 */
public class Animation extends SurfaceView implements SurfaceHolder.Callback, Runnable {


    public static final int DIR_L = 2, DIR_R = 3, DIR_U = 1, DIR_D = 0, DIR_NULL = -1;

    /** Used Thread to launch the animation */
    private Thread t ;
    /** Actual direction of the animation */
    private int dir = 1;
    /** Position X and Y of the animation on the screen */
    private int aX=100, aY=100;
    private Rect posS=new Rect();
    /** Image source of the animation */
    private Bitmap src_img;
    /** Dimension x and y of a part of the source image */
    private int sX=0, sY=0;
    private Rect[][] posImg ;
    /** Index of Horizontal Image */
    private int ind = 0;

    public Animation(Context context) {
        super(context);
        getHolder().addCallback(this);
        src_img = BitmapFactory.decodeResource(getResources(),R.drawable.anime);
        sX = src_img.getWidth() /4;
        sY = src_img.getHeight() /4;
        posImg = new Rect[4][4];
        for(int i=0;i<4;i++)
            for(int j=0;j<4;j++)
                posImg[i][j]= new Rect(j*sX,i*sY,(j+1)*sX-1,(i+1)*sY-1);
    }

    public void setDir(int d){ dir=d; }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawColor(Color.BLACK);
        canvas.drawBitmap(src_img,posImg[dir][ind],posS, null);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        aX = width / 2 - sX / 2;
        aY = height / 2 - sY / 2;
        posS = new Rect(aX,aY,aX+sX,aY+sY);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Log.d("surfaceCreated", "fabriquée");
        drawAnimation();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
    }

    public void drawAnimation(){
        Canvas canvas = null;
        SurfaceHolder sh =null;
        try {
            sh = getHolder();
            canvas = sh.lockCanvas(null);
            synchronized (sh) {
                this.onDraw(canvas);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (canvas != null && sh!=null) {
                sh.unlockCanvasAndPost(canvas);
            }
        }
    }

    @Override
    public void run(){
        try{
            while(true){
                Log.d("drawing","drawing begin "+ind);
                drawAnimation();
                ind = (ind+1) %4;
                Thread.sleep(50);
            }
        }catch (InterruptedException e){
        }
    }

    public void interrupt(){
        if(t!=null)
            t.interrupt();
    }

    public void start(){
        if(t==null)
            t = new Thread(this);
        t.start();
    }
}