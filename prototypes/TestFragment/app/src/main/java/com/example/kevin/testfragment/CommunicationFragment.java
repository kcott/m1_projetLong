package com.example.kevin.testfragment;

import android.app.Activity;
import android.support.v4.app.ListFragment;

/**
 * Created by Kevin on 06/03/2015.
 */
public class CommunicationFragment extends ListFragment{
    CommunicationFragmentListener fragment;

    public interface CommunicationFragmentListener{
        public void onSelected(int position);
    }
    @Override
    public void onAttach(Activity activity)throws ClassCastException{
        super.onAttach(activity);
        if (activity instanceof CommunicationFragmentListener){
            fragment = (CommunicationFragmentListener)activity;
        }else{
            throw new ClassCastException(activity.toString());
        }
    }
}
