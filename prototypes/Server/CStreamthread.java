import java.net.*;
import java.io.*;

class CStreamThread extends StreamThread {
    
    private Client client;
    
    public CStreamThread(Player p, Client c){
	super(p);
	client=c;
    }

    public void event(int i){
	System.out.println("<-- "+i+" "+((char)i)+" -->");
	try{
	    switch(i){
	    case -1  : lost(); break;
	    case 'P' : 
		System.out.println((clock.ping()/1000000.0)+" ms");
		break;
	    case 'n' : player.write(player.name); break;
	    case 'c' : player.write(0); break;
	    case 'l' : System.out.println(player.readLine()); break;
	    case 'N' :
		System.out.println("Your login is already used.");
		lost();
		break;
	    default:
	    }
	}catch(Exception e){}
    }

    public void alert(){
	try{
	    player.write('c');
	}catch(Exception e){ System.out.println("Error write CST alert"); }
    }

    public void lost(){
	interrupt();
	client.close();
    }

    public void ping(){
	try{
	    player.write('p');
	}catch(Exception e){ System.out.println("Error write ping"); }
    }
}
