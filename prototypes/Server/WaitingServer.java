import java.io.*;
import java.net.*;
import java.util.*;

public class WaitingServer {
    public Map<String,Player> current_player = new HashMap<String,Player>();

    public void addPlayer(Socket skt){
	Player p = new Player(skt);
	p.askForName();
	System.out.println("New Player : "+p.name);
	synchronized(current_player){
	    if(current_player.get(p.name)!=null){
		try{
		    p.write('N');
		    p.close();
		}catch(Exception e){
		    System.out.println("Error WS addPlayer");}
	    }else{
		current_player.put(p.name,p);
		setDialog(p);
	    }
	}
    }

    public void setDialog(Player p){
	(new WStreamThread(p,this)).start();
    }

    public void changePlayerStatus(int stat, Player p){
	if(stat==Player.DISCONNECTED || stat==Player.LOST){
	    System.out.println("Player lost or disconnected : "+p.name);
	    synchronized(current_player){
		current_player.remove(p.name);
	    }
	    p.close();
	}else
	    p.status=stat;
    }

    public void sendAll(String g) throws IOException{
	for(Player p : current_player.values()){
	    if(p.status==Player.FREE)
		p.write("l"+g);
	}
    }


}
