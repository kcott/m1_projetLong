import java.net.*;
import java.io.*;

class WStreamThread extends StreamThread {

    private WaitingServer waitserver;

    public WStreamThread(Player p, WaitingServer ws){
	super(p);
	waitserver=ws;
    }
    
    public void event(int i){
	System.out.println("<-- "+i+" "+(char)i+" -->");
	try{
	    switch(i){
	    case 'p' : player.write('P'); break;
	    case 'c' : player.write(0); break;
	    case -1 :
		waitserver.changePlayerStatus(Player.DISCONNECTED,player);
		interrupt(); break;
	    case 'l' :
		System.out.println(player.readLine());
		break;
	    case 'g' :
		if(player.status == Player.IN_GAME)
		    waitserver.changePlayerStatus(Player.FREE, player);
		else waitserver.changePlayerStatus(Player.IN_GAME, player);
		break;
	    default:
	    }
	}catch(IOException e){}
    }

    public void alert(){
	try{
	    player.write('c');
	}catch(Exception e){ System.out.println("Error write WST alert");}
    }

    public void lost(){
	waitserver.changePlayerStatus(Player.LOST,player);
	interrupt();
    }

    public void ping(){}

}
