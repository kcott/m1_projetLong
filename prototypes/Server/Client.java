import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class Client {

    Player player;
    Scanner sc = new Scanner(System.in);

    public Client(String n){
        try {
            Socket skt = new Socket("localhost",3000);
	    player = new Player(skt,n);
	    System.out.println("Client "+player.name+" : Connecté au serveur");
	}catch (IOException ex){
	    System.out.println("Error to connect on server.");
	    close();
        }
    }

    public void close(){
	if(sc!=null) sc.close();
	if(player!=null) player.close();
    }

    public void run(){
	(new CStreamThread(player,this)).start();
	try{
	    String g = null;
	    while(true){
		g=sc.nextLine();
		if(g.equals("g"))
		    player.write('g');
		else if(g.equals("disconnect")){
		    player.write(-1);
		    close();
		    break;
		}else 
		    player.write('l'+g+'\n');
	    }
	}catch(Exception e){
	    System.out.println("Closing player");
	    close();
	}
	System.out.println("Deconnexion");
    }


    public static void main(String[] a){
	Client c = new Client(a[0]);
	c.run();
    }    

}
