public class Clock extends Thread {

    public int clock = 0, MAX_CLOCK = 6,wait_time=500;
    private Stoppable st =null;
    private long ping_beg, ping_end;

    public Clock(Stoppable s){
	st=s;
    }

    public void run(){
	try{
	    while(true){
		synchronized(this){
		    if(clock>=MAX_CLOCK/2)  st.alert();
		    if(clock<MAX_CLOCK)     clock++;
		    else	            st.lost();
		}
		if(ping_beg<=ping_end){
		    st.ping();
		    ping_beg=System.nanoTime();
		}
		Thread.sleep(wait_time);
	    }
	}catch(Exception e){}
    }

    public long ping(){
	ping_end=System.nanoTime();
	return ping_end - ping_beg ;
    }
    
}
