import java.net.ServerSocket;
import java.net.Socket;

public class MainServer {
    
    private static WaitingServer ws = new WaitingServer();

    public static void main(String args[]) {
	ServerSocket srvr=null;
	try {
	    srvr = new ServerSocket(3000);
	}
	catch(Exception e) {
	    System.out.println("Fail to launch server.");
	}
	System.out.println("server launched with success.");
	
	while (true) {
	    try {
		Socket skt = srvr.accept();
		ws.addPlayer(skt);
	    }
	    catch(Exception e) {
		System.out.println("Fail to connect new player.");
	    }
	}
    }
    
}
