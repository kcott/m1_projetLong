import java.io.*;
import java.net.*;


public class Player {

    public String name=null;
    private Socket skt;
    public int status = FREE;
    private BufferedReader in = null;
    private BufferedWriter out= null;
    public static final int FREE = 0,
	IN_GAME = 1,
	LOST=2,
	DISCONNECTED=3;

    public Player(Socket s){
	this.skt=s;
	try{
	    in=new BufferedReader(new InputStreamReader(skt.getInputStream()));
	    out=new BufferedWriter(new OutputStreamWriter(skt.getOutputStream()));
	}catch(Exception e){
	    close();
	    System.out.println("Fail to attach reader and writer.");
	}
    }

    public Player(Socket s, String n){
	this(s);
	name=n;
    }

    public void askForName(){
	try{
	    write('n');
	    name= readLine();
	}catch(Exception e){
	    System.out.println("Error while asking for player's name");
	    e.printStackTrace();
	}
    }

    public void close(){
	try{
	    if(skt!=null) skt.close();
	    if(in!=null)  in.close();
	    if(out!=null) out.close();
	}catch(Exception e){}
	System.out.println("Player Fermé");
    }
    
    public int read() throws IOException{ return in.read(); }

    public String readLine() throws IOException{ return in.readLine(); }

    public void write(int i) throws IOException{
	synchronized(out){ out.write(i); out.flush(); } }

    public void write(String g) throws IOException{
	synchronized(out){ out.write(g+"\n"); out.flush(); } }

}
