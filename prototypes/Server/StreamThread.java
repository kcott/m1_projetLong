import java.io.*;

public abstract class StreamThread extends Thread implements Stoppable{
    protected Player player =null;
    protected Clock clock;
    
    public StreamThread(Player p, Stoppable s){
	super();
	this.player=p;
	clock= new Clock(s);
    }

    public StreamThread(Player p){
        super();
	this.player=p;
	clock= new Clock(this);
    }

    public void run(){
	clock.start();
	try{
	    while(true){
		event(player.read());
		synchronized(clock){ clock.clock=0; }
	    }
	}catch(Exception e){
	    System.out.println("Error read ST : "+player.name);
	    interrupt();
	}
    }

    public void interrupt(){
	System.out.println("ST interrupt");
	clock.interrupt();
	super.interrupt();
    }

    public abstract void event(int i);

}
