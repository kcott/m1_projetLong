public interface Stoppable {
    
    public void alert();
    public void lost();
    public void ping();

}
