package application;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import user_interface.controller.AskingNameMenu;
import user_interface.controller.PreparationGame;
import user_interface.controller.SettingsActivity;

public class MainActivity extends Activity {

    private int REQUEST_PLAYER_NAME = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menu);

        // Gets buttons to add events
        Button bContinu    = (Button) findViewById(R.id.bContinu);
        bContinu.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(MainActivity.this, PreparationGame.class);
                startActivity(i);
            }
        });
        Button bNewgame    = (Button) findViewById(R.id.bNewgame);
        bNewgame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, AskingNameMenu.class);
                startActivityForResult(i, REQUEST_PLAYER_NAME);
            }
        });
        Button bMultiplayer= (Button) findViewById(R.id.bMultiplayer);
        Button bSettings   = (Button) findViewById(R.id.bSettings);
        bSettings.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(MainActivity.this, SettingsActivity.class);
                startActivity(i);
            }
        });
        Button bExit       = (Button) findViewById(R.id.bExit);
        bExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_PLAYER_NAME){
            if(resultCode == Activity.RESULT_OK){
                Log.d("Player name =", data.getStringExtra("name"));
                Intent i = new Intent(MainActivity.this, PreparationGame.class);
                startActivity(i);
            }
        }
    }

}
