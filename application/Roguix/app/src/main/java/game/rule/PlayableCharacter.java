package game.rule;

import android.graphics.Bitmap;

import java.util.List;

import engine.CriteriaEngine;
import engine.multimedia.MovingRepresentation;
import engine.multimedia.RepresentationFactory;
import engine.physics_engine.CollisionMask2D;
import engine.physics_engine.CollisionMaskFactory;
import game.rule_game.*;

/**
 * Created by cottin on 19/05/15.
 */
public class PlayableCharacter extends GameCharacter{
    public PlayableCharacter(){
        super();
        // Instantiate general characteristics
        setAttribute("force", new Attribute("force", 10, 0, 1000000));
        setAttribute("dexterity", new Attribute("dexterity", 10, 0, 1000000));
        setAttribute("agility", new Attribute("agility", 10, 0, 1000000));
        setAttribute("intelligence", new Attribute("intelligence", 10, 0, 1000000));

        // Instantiate detailed characteristics
        setAttribute("pv", new Attribute("intelligence", 10, 0, 1000000));
        setAttribute("pm", new Attribute("intelligence", 10, 0, 1000000));
        setAttribute("physicdamage", new Attribute("physicdamage", 10, 0, 1000000));
        setAttribute("magicdamage", new Attribute("magicdamage", 10, 0, 1000000));
        setAttribute("physicdefense", new Attribute("physicdefense", 10, 0, 1000000));
        setAttribute("magicdefense", new Attribute("magicdefense", 10, 0, 1000000));

        // Instantiate auxiliaries characteristics
        setAttribute("experience", new Attribute("experience", 10, 0, 1000000));
        setAttribute("money", new Attribute("money", 10, 0, 1000000));
    }
    public void instanciate(
            List<CriteriaEngine> criterias,
            Bitmap imageDown,
            Bitmap imageUp,
            Bitmap imageLeft,
            Bitmap imageRight){
        setRepresentation("down", RepresentationFactory.newPhysicsEngine(criterias));
        if(getRepresentation("down") instanceof MovingRepresentation) {
            MovingRepresentation r2d = (MovingRepresentation) getRepresentation("down");
            r2d.setRepresentation(imageDown, 3, 32, 48);
            r2d.setFixedRepresentation(0);
            for(int n=0; n<r2d.getNumberFixedRepresentation(); n++){
                CollisionMask2D mask = CollisionMaskFactory.newCollisionMask(criterias);
                mask.createMask(r2d.getFixedRepresentation(n).getRepresentation());
                r2d.getFixedRepresentation(0).setCollisionMask(mask);
            }
        }
        setRepresentation("up", RepresentationFactory.newPhysicsEngine(criterias));
        if(getRepresentation("up") instanceof MovingRepresentation) {
            MovingRepresentation r2d = (MovingRepresentation) getRepresentation("up");
            r2d.setRepresentation(imageUp, 3, 32, 48);
            r2d.setFixedRepresentation(0);
            for(int n=0; n<r2d.getNumberFixedRepresentation(); n++){
                CollisionMask2D mask = CollisionMaskFactory.newCollisionMask(criterias);
                mask.createMask(r2d.getFixedRepresentation(n).getRepresentation());
                r2d.getFixedRepresentation(0).setCollisionMask(mask);
            }
        }
        setRepresentation("left", RepresentationFactory.newPhysicsEngine(criterias));
        if(getRepresentation("left") instanceof MovingRepresentation) {
            MovingRepresentation r2d = (MovingRepresentation) getRepresentation("left");
            r2d.setRepresentation(imageLeft, 3, 32, 48);
            r2d.setFixedRepresentation(0);
            for(int n=0; n<r2d.getNumberFixedRepresentation(); n++){
                CollisionMask2D mask = CollisionMaskFactory.newCollisionMask(criterias);
                mask.createMask(r2d.getFixedRepresentation(n).getRepresentation());
                r2d.getFixedRepresentation(0).setCollisionMask(mask);
            }
        }
        setRepresentation("right", RepresentationFactory.newPhysicsEngine(criterias));
        if(getRepresentation("right") instanceof MovingRepresentation) {
            MovingRepresentation r2d = (MovingRepresentation) getRepresentation("right");
            r2d.setRepresentation(imageRight, 3, 32, 48);
            r2d.setFixedRepresentation(0);
            for(int n=0; n<r2d.getNumberFixedRepresentation(); n++){
                CollisionMask2D mask = CollisionMaskFactory.newCollisionMask(criterias);
                mask.createMask(r2d.getFixedRepresentation(n).getRepresentation());
                r2d.getFixedRepresentation(0).setCollisionMask(mask);
            }
        }
        setCurrentRepresentation("down");
    }
}
