package game.rule_game;

/**
 * Created by cottin on 11/05/15.
 */
public class Attribute {
    private String name;
    private double value;
    private double minvalue;
    private double maxvalue;
    public Attribute(String name, double value, double minvalue, double maxvalue){
        setName(name);
        this.minvalue = minvalue;
        if(minvalue < maxvalue)
            this.maxvalue = maxvalue;
        else
            this.maxvalue = minvalue;
        setValue(value);
    }
    public Attribute(double value, double minvalue, double maxvalue){
        this("", value, minvalue, maxvalue);
    }
    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }
    public double getValue(){
        return value;
    }
    public void setValue(double value){
        if(value < minvalue)
            this.value = minvalue;
        else if(value < maxvalue)
            this.value = value;
        else
            this.value = maxvalue;
    }
    public double getMinValue(){
        return minvalue;
    }
    public void setMinValue(double minvalue){
        if(minvalue < this.minvalue)
            this.minvalue = minvalue;
        else if(minvalue < maxvalue){
            this.minvalue = minvalue;
            setValue(value);
        }else
            this.minvalue = maxvalue;
    }
    public double getMaxValue(){
        return maxvalue;
    }
    public void setMaxValue(double maxvalue){
        if(maxvalue > this.maxvalue)
            this.maxvalue = maxvalue;
        else if(maxvalue > minvalue){
            this.maxvalue = maxvalue;
            setValue(value);
        }else
            this.maxvalue = minvalue;
    }
}
