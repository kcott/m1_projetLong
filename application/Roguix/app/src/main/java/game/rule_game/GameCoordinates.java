package game.rule_game;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by cottin on 13/05/15.
 */
public class GameCoordinates {
    private Map<String,Number> points;
    public GameCoordinates(Map<String, Number> points){
        this.points = points;
    }
    public GameCoordinates(){
        this(new HashMap<String, Number>());
    }
    public Number getPoint(String key){
        return points.get(key);
    }
    public void setPoint(String key, Number number){
        points.put(key, number);
    }
}
