package game.rule_game;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by cottin on 11/05/15.
 */
public class GameCharacter extends Element{
    private String name;
    private Map<String, Attribute> attributes;

    public GameCharacter(){
        super();
        attributes = new HashMap<String,Attribute>();
    }

    public boolean isAlive(Number attribute){
        if (0 < (double) attribute)
            return true;
        return false;
    }
    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }
    public Attribute getAttribute(String key){
        return attributes.get(key);
    }
    public void setAttribute(String key, Attribute attribute){
        attributes.put(key, attribute);
    }
}
