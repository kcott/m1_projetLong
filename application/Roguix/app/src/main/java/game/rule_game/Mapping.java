package game.rule_game;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import engine.multimedia.Representation;

/**
 * Created by cottin on 11/05/15.
 */
public class Mapping extends Element{
    private Tile[][] tiles;
    private Map<String,GameCharacter> gameCharacters;
    private Map<String,Item> items;
    private Map<String,Action> actions;

    private int widthElement;
    private int heightElement;
    private int width;
    private int height;

    public Mapping(int width, int height){
        super();

        widthElement  = 0;
        heightElement = 0;

        this.width  = width;
        this.height = height;

        tiles = new Tile[width][height];
        for(int x=0; x<width; x++)
            for(int y=0; y<height; y++)
                tiles[x][y] = new Tile();
        setPositionTiles();

        gameCharacters = new HashMap<String,GameCharacter>();
        items = new HashMap<String,Item>();
        actions = new HashMap<String,Action>();
    }
    public Tile getTile(int x, int y){
        return tiles[x][y];
    }
    public void setTiles(int x, int y, Tile tile){
        tiles[x][y] = tile;
    }
    public GameCharacter getGameCharacter(String key){
        return gameCharacters.get(key);
    }
    public void setGameCharacter(String key, GameCharacter character){
        gameCharacters.put(key, character);
    }
    public Item getItem(String key){
        return items.get(key);
    }
    public void setItem(String key, Item item){
        items.put(key, item);
    }
    public Action getAction(String key){
        return actions.get(key);
    }
    public void setAction(String key, Action action){
        actions.put(key, action);
    }
    public int getHeight(){
        return height;
    }
    public int getWidth(){
        return width;
    }
    public void setRepresentationTiles(
            Representation representation,
            int x,
            int y,
            int width,
            int height){
        for (int xx=Math.max(x,0); xx<Math.min(width,this.width); xx++){
            for (int yy=Math.max(y,0); yy<Math.min(height,this.height); yy++){
                tiles[xx][yy].setRepresentation("tile",representation);
            }
        }
    }
    private void setPositionTiles(){
        for (int x=0; x<width; x++){
            for (int y=0; y<height; y++){
                tiles[x][y].setCoordinates(new GameCoordinates());
                tiles[x][y].getCoordinates().setPoint("x",x*widthElement);
                tiles[x][y].getCoordinates().setPoint("y",y*heightElement);
            }
        }
    }
    public void updatePositionTiles(){
        for (int x=0; x<width; x++){
            for (int y=0; y<height; y++){
                tiles[x][y].setCoordinates(new GameCoordinates());
                tiles[x][y].getCoordinates().setPoint("x",x*widthElement);
                tiles[x][y].getCoordinates().setPoint("y",y*heightElement);
            }
        }
    }
    public int getWidthElement(){
        return widthElement;
    }
    public int getHeightElement(){
        return heightElement;
    }
    public void setWidthElement(int width){
        widthElement = width;
        setPositionTiles();
    }
    public void setHeightElement(int height){
        heightElement = height;
        setPositionTiles();
    }
}
