package game.rule_game;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by cottin on 11/05/15.
 */
public class Item extends Element{
    private Map<String,Action> actions;
    public Item(){
        super();
        actions = new HashMap<String,Action>();
    }
    public Action getAction(String key){
        return actions.get(key);
    }
    public void setAction(String key, Action action){
        this.actions.put(key, action);
    }
}
