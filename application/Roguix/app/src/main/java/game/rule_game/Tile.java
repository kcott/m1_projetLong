package game.rule_game;

/**
 * Created by cottin on 21/05/15.
 */
public class Tile extends Element {
    private int value;
    public Tile(){
        super();
    }
    public int getValue() {
        return value;
    }
    public void setValue(int value) {
        this.value = value;
    }
}
