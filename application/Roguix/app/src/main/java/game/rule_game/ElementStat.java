package game.rule_game;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by cottin on 11/05/15.
 */
public class ElementStat {
    private String name;
    private Map<String,Number> stats;
    public ElementStat(String name, Map<String, Number> stats){
        this.name = name;
        this.stats = stats;
    }
    public ElementStat(String name){
        this.name = name;
        stats = new HashMap<String,Number>();
    }
    public ElementStat(Map<String, Number> stats){
        name = "";
        this.stats = stats;
    }
    public ElementStat(){
        name = "";
        stats = new HashMap<String,Number>();
    }
    public String getName(){
	    return name;
    }
    public void setName(String name){
	    this.name = name;
    }
    public Number getStat(String key){
	    return stats.get(key);
    }
    public void setStat(String key, Number number){
	    stats.put(key,number);
    }
}
