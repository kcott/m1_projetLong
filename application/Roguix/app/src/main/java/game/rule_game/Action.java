package game.rule_game;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by cottin on 11/05/15.
 */
public class Action extends Element{
    public List<Event> events;
    public Action(){
        super();
        events = new LinkedList<Event>();
    }
}
