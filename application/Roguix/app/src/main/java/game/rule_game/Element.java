package game.rule_game;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import engine.physics_engine.CollisionMask;
import engine.multimedia.Representation;

/**
 * Created by cottin on 11/05/15.
 */
public abstract class Element {
    private String name;
    private GameCoordinates coordinates;
    private String description;
    private Map<String,ElementStat> stats;
    private CollisionMask collisionMask;
    private Map<String,Representation> representations;
    private String currentRepresentation;

    public Element(){
        stats = new HashMap<String,ElementStat>();
        representations = new HashMap<String,Representation>();
    }
    public String getName(){
	    return name;
    }
    public void setName(String name){
	    this.name = name;
    }
    public GameCoordinates getCoordinates(){
	    return coordinates;
    }
    public void setCoordinates(GameCoordinates coordinates){
	    this.coordinates = coordinates;
    }
    public String getDescription(){
	    return description;
    }
    public void setDescription(String description){
	    this.description = description;
    }
    public ElementStat getElementStat(String key){
	    return stats.get(key);
    }
    public void setElementStat(String key, ElementStat stat){
	    this.stats.put(key, stat);
    }
    public CollisionMask getCollisionMask(){
	    return collisionMask;
    }
    public void setCollisionMask(CollisionMask mask){
	    this.collisionMask = mask;
    }
    public Representation getRepresentation(String key){
	    return representations.get(key);
    }
    public void setRepresentation(String key, Representation representation){
	    representations.put(key, representation);
    }
    public String getCurrentRepresentation() {
        return currentRepresentation;
    }
    public void setCurrentRepresentation(String currentRepresentation) {
        if(representations.get(currentRepresentation)!=null)
            this.currentRepresentation = currentRepresentation;
    }
    public void update(){

    }
}
