package game.rule_game;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by cottin on 11/05/15.
 */
public class Environment extends Element{
    private Map<String,GameCharacter> gameCharacters;
    private Map<String,Item> items;
    private Map<String,Action> actions;
    private Map<String,Mapping> mappings;
    public Environment(){
        super();
        empty();
    }
    public GameCharacter getGameCharacter(String key){
	    return gameCharacters.get(key);
    }
    public void setGameCharacter(String key, GameCharacter character){
	    gameCharacters.put(key, character);
    }
    public Item getItem(String key){
	    return items.get(key);
    }
    public void setItem(String key, Item item){
        this.items.put(key, item);
    }
    public Action getAction(String key){
        return actions.get(key);
    }
    public void setActions(String key, Action action){
        this.actions.put(key, action);
    }
    public Mapping getMapping(String key){
        return mappings.get(key);
    }
    public void setMapping(String key, Mapping mapping){
        mappings.put(key, mapping);
    }
    public void empty(){
        gameCharacters = new HashMap<String,GameCharacter>();
        items = new HashMap<String,Item>();
        actions = new HashMap<String,Action>();
        mappings = new HashMap<String,Mapping>();
    }

    public void create(){ // XMLFile

    }
    public void add(Environment environment){
        gameCharacters.putAll(environment.gameCharacters);
        items.putAll(environment.items);
        actions.putAll(environment.actions);
        mappings.putAll(environment.mappings);
    }
    public void add(){ // XMLFile

    }
}
