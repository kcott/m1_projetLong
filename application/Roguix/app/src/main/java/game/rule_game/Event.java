package game.rule_game;

import java.util.List;

/**
 * Created by cottin on 11/05/15.
 */
public class Event extends Element{
    public Event(){
        super();
    }
    public void update(List<Element> elements){
        for(int i=0; i<elements.size(); i++){
            elements.get(i).update();
        }
    }
}
