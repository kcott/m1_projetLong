package game;

import android.graphics.Canvas;
import android.util.Log;
import android.view.SurfaceHolder;

import engine.multimedia.MovingRepresentation;
import game.rule.PlayableCharacter;

/**
 * Created by cottin on 11/05/15.
 */
public class GameRunner implements Runnable{
    public Game game;
    public int framePerSeconds = 30;

    public GameRunner(Game game){
        super();
        this.game = game;
    }
    @Override
    public void run(){
        try {
            int time = (int) 1000/framePerSeconds;
            while (true) {
                Thread.sleep(time);
                Canvas canvas = null;
                SurfaceHolder sh = null;
                try {
                    sh = game.getHolder();
                    canvas = sh.lockCanvas(null);
                    synchronized (sh) {
                        game.draw(canvas);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    break;
                } finally {
                    if (canvas != null && sh!=null) {
                        sh.unlockCanvasAndPost(canvas);
                    }
                }
                MovingRepresentation pPlayer = (MovingRepresentation) game.player.getRepresentation(game.player.getCurrentRepresentation());
                for(PlayableCharacter enemy:game.enemies) {
                    MovingRepresentation pEnemy = (MovingRepresentation) enemy.getRepresentation(enemy.getCurrentRepresentation());
                    if (pPlayer.getFixedRepresentation(0).getCollisionMask().isCollision(
                            pPlayer.getFixedRepresentation(0),
                            game.player.getCoordinates(),
                            pEnemy.getFixedRepresentation(0),
                            enemy.getCoordinates())) {
                        game.player.getAttribute("pv").setValue(game.player.getAttribute("pv").getValue() - 1);
                        if (game.player.getAttribute("pv").getValue() == 0) {
                            game.end();
                        }
                    }
                }
                game.postInvalidate();
            }
        }catch (Exception e){
            Log.d("GameRunner.start()", e.toString());
        }
    }
    public void pause(){

    }
}
