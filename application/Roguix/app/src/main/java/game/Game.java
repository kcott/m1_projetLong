package game;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import application.R;
import engine.CriteriaEngine;
import engine.Engine;
import engine.graphics_engine.GraphicsEngine2D;
import engine.multimedia.MovingRepresentation;
import engine.multimedia.Representation;
import engine.multimedia.RepresentationFactory;
import engine.physics_engine.CollisionMask2D;
import engine.physics_engine.CollisionMaskFactory;
import game.level.Level;
import game.player.GamePlayer;
import game.rule.PlayableCharacter;
import game.rule_game.GameCharacter;
import game.rule_game.GameCoordinates;
import game.rule_game.Mapping;
import user_interface.controller.GameActivity;
import user_interface.controller.Joystick;

/**
 * Created by cottin on 11/05/15.
 */
public class Game extends SurfaceView implements SurfaceHolder.Callback{
    public Engine engine;
    public Level level;
    public GamePlayer player;
    public Joystick joystick;
    public SurfaceHolder surfaceHolder;
    public DisplayMetrics screen;
    public int pxMove = 50;

    public final int MAX_NUMBER_ENEMIES = 20;

    public List<PlayableCharacter> enemies;

    public Game(Context context, DisplayMetrics screen, Joystick joystick){
        super(context);

        LinkedList<CriteriaEngine> criterias = new LinkedList<CriteriaEngine>();
        enemies = new LinkedList<PlayableCharacter>();
        criterias.add(CriteriaEngine.TWO_D);

        this.screen = screen;
        this.joystick = joystick;
        engine = new Engine(criterias);
        getHolder().addCallback(this);
        level = new Level();

        // Initialize player
        player = new GamePlayer();
        player.setCoordinates(new GameCoordinates());
        player.getCoordinates().setPoint("x", 100);
        player.getCoordinates().setPoint("y", 100);
        player.instanciate(
                criterias,
                getDrawableToBitmap(R.drawable.character1_front, 96, 48),
                getDrawableToBitmap(R.drawable.character1_back, 96, 48),
                getDrawableToBitmap(R.drawable.character1_left, 96, 48),
                getDrawableToBitmap(R.drawable.character1_right, 96, 48));

        // Initialize representations mapping
        Representation ground = RepresentationFactory.newPhysicsEngine(criterias);
        if(ground instanceof MovingRepresentation){
            MovingRepresentation r2d = ((MovingRepresentation) ground);
            r2d.setRepresentation(getDrawableToBitmap(R.drawable.grass, 32, 32), 1, 32, 32);
            r2d.setFixedRepresentation(0);
            for(int n=0; n<r2d.getNumberFixedRepresentation(); n++){
                CollisionMask2D mask = CollisionMaskFactory.newCollisionMask(criterias);
                mask.createMask(r2d.getFixedRepresentation(n).getRepresentation());
                r2d.getFixedRepresentation(0).setCollisionMask(mask);
            }
        }

        // Initialize mapping
        level.getEnvironment().getMapping("start").setHeightElement(32);
        level.getEnvironment().getMapping("start").setWidthElement(32);
        level.getEnvironment().getMapping("start").setRepresentationTiles(
                ground,
                0,
                0,
                level.getEnvironment().getMapping("start").getWidth(),
                level.getEnvironment().getMapping("start").getHeight());

        // Initialize enemiez
        createEnemiesForTest(criterias);
    }

    private void createEnemiesForTest(List<CriteriaEngine> criterias){
        Random r = new Random();
        Mapping m = level.getEnvironment().getMapping("start");
        Bitmap down = getDrawableToBitmap(R.drawable.character2_front, 96, 48);
        Bitmap up = getDrawableToBitmap(R.drawable.character2_back, 96, 48);
        Bitmap left = getDrawableToBitmap(R.drawable.character2_left, 96, 48);
        Bitmap right = getDrawableToBitmap(R.drawable.character2_right, 96, 48);
        for (int i=0; i<(Math.max(1,Math.abs(r.nextInt()%MAX_NUMBER_ENEMIES))); i++) {
            Random rx = new Random();
            Random ry = new Random();
            PlayableCharacter enemy;
            enemy = new PlayableCharacter();
            enemy.setCoordinates(new GameCoordinates());
            enemy.getCoordinates().setPoint("x",
                    Math.min(m.getWidth() * m.getWidthElement() - 48,
                            Math.abs(rx.nextInt()) % (m.getWidth() * m.getWidthElement())));
            enemy.getCoordinates().setPoint("y",
                    Math.min(m.getHeight() * m.getHeightElement() - 96,
                            Math.abs(ry.nextInt()) % (m.getHeight() * m.getHeightElement())));
            enemy.instanciate(criterias, down, up, left, right);
            enemies.add(enemy);
        }
    }
    public Bitmap getDrawableToBitmap(int idDrawable, int width, int height){
        // Create bitmap from id drawable
        Bitmap bitmap = BitmapFactory.decodeResource(
                getResources(),
                idDrawable);
        // Bitmap size are often incorrect, resize to have correct size
        bitmap = Bitmap.createScaledBitmap(bitmap, width, height, true);
        return bitmap;
    }
    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if(engine.graphicsEngine instanceof GraphicsEngine2D) {
            GraphicsEngine2D g2d = (GraphicsEngine2D) engine.graphicsEngine;
            moveCharacter(player);
            Mapping map = level.getEnvironment().getMapping("start");

            // Find x values on grid mapping to display
            int xStart, xEnd;
            int xStartScreen, xEndScreen, xCaseStartScreen, xCaseEndScreen;
            xStartScreen = player.getCoordinates().getPoint("x").intValue() - screen.widthPixels/2;
            xEndScreen = player.getCoordinates().getPoint("x").intValue() + screen.widthPixels/2;
            xCaseStartScreen = (int) Math.ceil(xStartScreen / map.getWidthElement());
            xCaseEndScreen = (int) Math.floor(xEndScreen / map.getWidthElement());
            if(0 < xCaseStartScreen){
                xStart = xCaseStartScreen;
            }else{
                xStart = 0;
            }
            if(map.getWidth() < xCaseEndScreen){
                xEnd = xCaseEndScreen;
            }else{
                xEnd = map.getWidth();
            }

            // Find y values on grid mapping to display
            int yStart, yEnd;
            int yStartScreen, yEndScreen, yCaseStartScreen, yCaseEndScreen;
            yStartScreen = player.getCoordinates().getPoint("y").intValue() - screen.heightPixels/2;
            yEndScreen = player.getCoordinates().getPoint("y").intValue() + screen.heightPixels/2;
            yCaseStartScreen = (int) Math.ceil(yStartScreen / map.getHeightElement());
            yCaseEndScreen = (int) Math.floor(yEndScreen / map.getHeightElement());
            if(0 < yCaseStartScreen){
                yStart = yCaseStartScreen;
            }else{
                yStart = 0;
            }
            if(map.getHeight() < yCaseEndScreen){
                yEnd = yCaseEndScreen;
            }else{
                yEnd = map.getHeight();
            }
            g2d.drawMap(canvas, map, xStart, yStart, xEnd, yEnd, xStartScreen, yStartScreen);
            for(PlayableCharacter enemy:enemies) {
                g2d.drawElement(canvas, enemy, enemy.getCurrentRepresentation(), xStartScreen, yStartScreen);
            }
            g2d.drawElement(canvas, player, player.getCurrentRepresentation(), xStartScreen, yStartScreen);

            Paint pText = new Paint();
            pText.setColor(Color.WHITE);
            pText.setTextSize(20);
            canvas.drawText(
                    "PV "+(int)player.getAttribute("pv").getValue(),
                    20,
                    20,
                    pText);
        }
        joystick.draw(canvas);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        surfaceHolder = holder;
        Canvas canvas = null;
        try {
            // Add canvas
            canvas = holder.lockCanvas();
            synchronized (holder) {
                draw(canvas);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (canvas != null) {
                holder.unlockCanvasAndPost(canvas);
            }
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }
    @Override
    public boolean onTouchEvent(MotionEvent event){
        if(event.getAction() == MotionEvent.ACTION_DOWN
           || event.getAction() == MotionEvent.ACTION_MOVE) {
            int x = (int) event.getX();
            int y = (int) event.getY();
            if (joystick.isTouched(x, y)) {
                joystick.setRatio(x, y);
            } else {
                joystick.setRatio(joystick.getX(), joystick.getY());
            }
            try {
                Thread.sleep(5);
            } catch (Exception e) {
                Log.d("Game.onTouchEvent()", e.toString());
            }
        }else if(event.getAction() == MotionEvent.ACTION_UP){
            joystick.setRatio(joystick.getX(), joystick.getY());
        }
        return true;
    }
    public void moveCharacter(GameCharacter character){
        // Update graphics
        double dx = Math.abs(joystick.getDx());
        double dy = Math.abs(joystick.getDy());
        if(joystick.getDx() == 0 && joystick.getDy() == 0){

        }else if(0 < joystick.getDx() && 0 < joystick.getDy()) {
            // UP LEFT
            if(dx < dy)
                character.setCurrentRepresentation("up");
            else
                character.setCurrentRepresentation("left");
        }else if(0 < joystick.getDx() && joystick.getDy() < 0) {
            // DOWN LEFT
            if(dx < dy)
                character.setCurrentRepresentation("down");
            else
                character.setCurrentRepresentation("left");
        }else if(joystick.getDx() < 0 && 0 < joystick.getDy()) {
            // UP RIGHT
            if(dx < dy)
                character.setCurrentRepresentation("up");
            else
                character.setCurrentRepresentation("right");
        }else {
            // DOWN RIGHT
            if(dx < dy)
                character.setCurrentRepresentation("down");
            else
                character.setCurrentRepresentation("right");
        }
        // Update coordinates
        character.getCoordinates().setPoint("x",
                character.getCoordinates().getPoint("x").intValue() - joystick.getDx() * pxMove);
        character.getCoordinates().setPoint("y",
                character.getCoordinates().getPoint("y").intValue() - joystick.getDy() * pxMove);
    }
    public void end(){
        ((GameActivity)getContext()).finish();
    }
}
