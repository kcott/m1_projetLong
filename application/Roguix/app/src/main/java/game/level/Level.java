package game.level;

import game.rule_game.Environment;
import game.rule_game.Mapping;

/**
 * Created by cottin on 11/05/15.
 */
public class Level {
    private LevelStat stat;
    private Environment environment;
    public Level(Environment environment,LevelStat stat){
        this.environment = environment;
        this.stat = stat;
    }
    public Level(Environment environment){
        this(environment, new LevelStat());
    }
    public Level(){
        this(new Environment(), new LevelStat());
        this.environment.setMapping("start",new Mapping(20,10));
    }
    public LevelStat getStat() {
        return stat;
    }
    public void setStat(LevelStat stat) {
        this.stat = stat;
    }
    public Environment getEnvironment() {
        return environment;
    }
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }
}
