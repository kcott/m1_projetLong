package game.level;

import java.util.HashMap;
import java.util.Map;

import game.rule_game.Element;

/**
 * Created by cottin on 11/05/15.
 */
public class LevelStat {
    public String name;
    public Map<String,Number> stats;
    public LevelStat(String name, Map<String, Number> stats){
        this.name = name;
        this.stats = stats;
    }
    public LevelStat(String name){
        this.name = name;
        stats = new HashMap<String,Number>();
    }
    public LevelStat(Map<String, Number> stats){
        name = "";
        this.stats = stats;
    }
    public LevelStat(){
        name = "";
        stats = new HashMap<String,Number>();
    }
}
