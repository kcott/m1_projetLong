package game;

/**
 * Created by cottin on 11/05/15.
 */
public enum StateGame {
    CALLING,
    PAUSE,
    PLAYING,
    PREPARING,
    SEARCHING_CONNECTION,
    WAITING_CONNECTION
}
