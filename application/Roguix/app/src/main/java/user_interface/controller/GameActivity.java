package user_interface.controller;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;

import game.Game;
import game.GameRunner;

/**
 * Created by cottin on 13/05/15.
 */
public class GameActivity extends Activity {
    public Game game;
    public Thread threadLoop;
    public Joystick joystick;
    public DisplayMetrics screen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Get screen size
        screen = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(screen);

        // Choose size to joystick
        int sizeJoystick = Math.max(screen.heightPixels/3, 200);

        // Initialize game
        joystick = new Joystick(
                sizeJoystick,
                sizeJoystick/2,
                screen.heightPixels-sizeJoystick/2);
        game = new Game(this, screen, joystick);
        setContentView(game);
        threadLoop = new Thread(new GameRunner(game));
        threadLoop.start();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        threadLoop.interrupt();
    }
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){
        super.onSaveInstanceState(savedInstanceState);
        //savedInstanceState.putSerializable("screen",screen);
    }
}
