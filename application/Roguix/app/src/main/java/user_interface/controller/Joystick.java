package user_interface.controller;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

/**
 * Created by cottin on 22/05/15.
 */
public class Joystick{
    private int x;
    private int y;
    private int x_ind;
    private int y_ind;
    private int size;
    private int radius;
    private int alpha;
    private Paint paint;

    private double dx;
    private double dy;

    public Joystick(int size, int x, int y, int alpha){
        this.x = x;
        this.x_ind = x;
        this.y = y;
        this.y_ind = y;
        this.size = size;
        this.radius = size/2;
        this.alpha = alpha;
        this.dx = 0;
        this.dy = 0;
    }
    public Joystick(int size, int x, int y){
        this(size, x, y, 120);
    }
    public void draw(Canvas canvas){
        // Draw the container
        paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.argb(alpha,255,255,255));
        canvas.drawCircle(x, y, radius, paint);

        // Draw the indicator
        paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.argb(alpha,255,255,255));
        canvas.drawCircle(x_ind, y_ind, (radius / 5), paint);
    }
    public int getX(){
        return x;
    }
    public int getY(){
        return y;
    }
    public int getSize(){
        return size;
    }
    public double getDx(){
        return dx;
    }
    public double getDy(){
        return dy;
    }
    public boolean isTouched(int x, int y){
        if(Math.pow(this.x - x,2) + Math.pow(this.y - y,2) <= Math.pow(radius,2))
            return true;
        return false;
    }
    public void setRatio(int x, int y){
        x_ind = x;
        y_ind = y;
        dx = ((double) this.x - x) / radius;
        dy = ((double) this.y - y) / radius;
    }
}
