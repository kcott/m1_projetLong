package user_interface.controller;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import application.R;

/**
 * Created by fc on 06/04/15.
 */
public class SelectCharacter extends Activity{

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.select_character);

        if(savedInstanceState!=null){
            //replace three characters from bundle
        }else{
            //build a new selection of three characters
        }

        // Gets buttons to add events
        Button bOk    = (Button) findViewById(R.id.sel_char_ok);
        Button bCancel= (Button) findViewById(R.id.sel_char_cancel);
        bOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent resultData = new Intent();
                //set the selected character in resultData
                setResult(Activity.RESULT_OK, resultData);
                finish();
            }
        });
        bCancel.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                finish();
            }
        });
    }

    @Override
    protected void onSaveInstanceState (Bundle outState){
        super.onSaveInstanceState(outState);
        //save three characters in bundle
    }
}
