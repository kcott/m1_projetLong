package user_interface.controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import application.R;

/**
 * Created by fc on 06/04/15.
 */
public class PreparationGame extends FragmentActivity {

    public final static int
            ID_ATTRIBUTE = 0,
            ID_CRAFT     = 1,
            ID_SHOP      = 2,
            ID_STOCK     = 3,
            ID_READY     = 4,
            ID_NULL      = -1;
    private int idlayout = ID_NULL;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.prepare_game);

        // Gets buttons to add events
        ImageButton bAttr = (ImageButton) findViewById(R.id.prep_g_bed_but);
        bAttr.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    putLayout(ID_ATTRIBUTE);
                }
        });
        ImageButton bcraft = (ImageButton) findViewById(R.id.prep_g_craft_but);
        bcraft.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    putLayout(ID_CRAFT);
                }
        });
        ImageButton bshop = (ImageButton) findViewById(R.id.prep_g_shop_but);
        bshop.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    putLayout(ID_SHOP);
                }
        });
        ImageButton bitem = (ImageButton) findViewById(R.id.prep_g_item_but);
        ImageButton bready = (ImageButton) findViewById(R.id.prep_g_ready_but);
        bready.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    putLayout(ID_READY);
                }
        });

        // Update default layout
        putLayout(ID_ATTRIBUTE);
    }

    @Override
    protected void onSaveInstanceState (Bundle outState){
        super.onSaveInstanceState(outState);
        //save three characters in bundle
    }

    protected void removeLayout(){
        if(idlayout != ID_NULL) {
            LinearLayout ly = (LinearLayout) findViewById(R.id.prep_game_mod_layout);
            ly.removeViewAt(0);
        }
    }

    public void putLayout(int id){
        LinearLayout ly = (LinearLayout) findViewById(R.id.prep_game_mod_layout);
        /* We select the next view to put from argument */
        View selectView = null;
        if(this.idlayout != ID_NULL)
            removeLayout();
        idlayout = id;
        switch(id){
            case ID_ATTRIBUTE :
                selectView = getLayoutInflater().inflate(R.layout.prep_g_home,ly,false);
                ly.addView(selectView);
                break;
            case ID_CRAFT :
                selectView = getLayoutInflater().inflate(R.layout.prep_g_craft, ly, false);
                ly.addView(selectView);
                break;
            case ID_SHOP :
                selectView = getLayoutInflater().inflate(R.layout.prep_g_shop, ly, false);
                ly.addView(selectView);
                break;
            case ID_STOCK :
                //selectView = getLayoutInflater().inflate(R.layout.prep_g_attributes, ly, false);
                break;
            case ID_READY :
                selectView = getLayoutInflater().inflate(R.layout.prep_g_ready, ly, false);
                ly.addView(selectView);
                Button ready1 = (Button) findViewById(R.id.prep_g_read_but1);
                ready1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(PreparationGame.this, GameActivity.class);
                        startActivity(i);
                        finish();
                    }
                });
                break;
            default :
                break;
        }
        /* if next View is not already showed then we remove the previous and put the new */
        /*if(selectView!=null && id!=idlayout){
            if(idlayout!=-1)
                removeLayout();
            idlayout = id;
            ly.addView(selectView);
        }*/
    }
}
