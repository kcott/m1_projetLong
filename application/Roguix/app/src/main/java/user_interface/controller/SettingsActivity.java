package user_interface.controller;

import android.app.Activity;
import android.os.Bundle;

import application.R;

/**
 * Created by Kevin on 22/03/2015.
 */
public class SettingsActivity extends Activity{
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.settings);
    }
}
