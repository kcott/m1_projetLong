package user_interface.controller;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import application.R;

/**
 * Created by fc on 30/03/15.
 */
public class AskingNameMenu extends Activity{
    private EditText edit;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.asking_name);

        edit = (EditText) findViewById(R.id.askname_edit);

        //Set text if activity was stopped
        if(savedInstanceState!=null){
            String n = savedInstanceState.getString("name","Player");
            edit.setText(n, TextView.BufferType.EDITABLE);
        }

        // Gets buttons to add events
        Button bOk     = (Button) findViewById(R.id.askn_ok);
        bOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!edit.getText().toString().equals("")) {
                    Intent resultData = new Intent();
                    resultData.putExtra("name", edit.getText().toString());
                    setResult(Activity.RESULT_OK, resultData);
                    finish();
                }
            }
        });
        Button bCancel = (Button) findViewById(R.id.askn_cancel);
        bCancel.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent resultData = new Intent();
                setResult(Activity.RESULT_CANCELED, resultData);
                finish();
            }
        });
    }

    @Override
    protected void onSaveInstanceState (Bundle outState){
        super.onSaveInstanceState(outState);
        outState.putString("name", edit.getText().toString());
    }
}
