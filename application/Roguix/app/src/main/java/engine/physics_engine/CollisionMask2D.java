package engine.physics_engine;

import android.graphics.Bitmap;

import engine.multimedia.FixedRepresentation;
import game.rule_game.GameCoordinates;

/**
 * Created by cottin on 20/05/15.
 */
public interface CollisionMask2D {
    public void createMask(Bitmap image);
    public boolean isCollision(
            FixedRepresentation r1,
            GameCoordinates gc1,
            FixedRepresentation r2,
            GameCoordinates gc2);
}
