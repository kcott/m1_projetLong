package engine.physics_engine;

import engine.EngineCoordinates;
import game.rule_game.Element;

/**
 * Created by cottin on 11/05/15.
 */
public interface PhysicsEngine2D{
    public boolean isCollision(
            CollisionMask2D mask1,
            EngineCoordinates c1,
            CollisionMask2D mask2,
            EngineCoordinates c2);
    public void move(Element e, EngineCoordinates c);
}
