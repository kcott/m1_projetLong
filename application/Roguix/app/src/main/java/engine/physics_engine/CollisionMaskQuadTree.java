package engine.physics_engine;

import android.graphics.Bitmap;
import android.graphics.Color;

import engine.multimedia.FixedRepresentation;
import engine.multimedia.Representation2D;
import game.rule_game.GameCoordinates;

class CollisionMaskQuadTree implements CollisionMask, CollisionMask2D{
	public static int DEFAULT_ALPHA = 123;
	private Tree tree;
	private int alpha;
	private int size;
	
	private Bitmap bi;
	private ColorQuadTree data[][];

	/**
	 * Create a mask to a image.
	 * @param image transform mask
	 * @param alpha of the mask
	 */
	public CollisionMaskQuadTree(Bitmap image,int alpha){
		this.alpha = alpha;
		bi = image;
		calculateSizeMask();
		calculateMask();
	}
	/**
	 * Create a mask to a image.
	 * @param image transform mask
	 */
	public CollisionMaskQuadTree(Bitmap image){
		this(image,DEFAULT_ALPHA);
	}
    /**
     * Create a mask to a image.
     */
    public CollisionMaskQuadTree(){
        this.alpha = DEFAULT_ALPHA;
    }
	/**
	 * Get the alpha used to calculate the mask.
	 * @return alpha
	 */

    public void createMask(Bitmap image){
        alpha = DEFAULT_ALPHA;
        bi = image;
        calculateSizeMask();
        calculateMask();
    }
	public int getAlpha(){
		return alpha;
	}
	/**
	 * Get the size of the mask.<br/>
	 * The width and the height is equals to the size.
	 * @return size of the mask
	 */
	public int getSizeMask(){
		return size;
	}
	/**
	 * Calculate a new mask.
	 * @param image transform mask
	 * @param alpha of the mask
	 */
	public void setTree(Bitmap image, int alpha){
		if(alpha != this.alpha && image != this.bi){
			this.alpha = alpha;
			this.bi = image;
			calculateMask();
		}
	}
	/**
	 * Get the mask in quad tree form.
	 * @return
	 */
	public Tree getTree(){
		return tree;
	}
	private void calculateMask(){
		// Get pixels of image
		data = new ColorQuadTree[size][size];
		for(int w=0; w<bi.getWidth(); w++){
			for(int h=0; h<bi.getHeight(); h++){
                int color = bi.getPixel(w, h);
				data[w][h] = new ColorQuadTree(
                        Color.alpha(color),
                        Color.red(color),
                        Color.green(color),
                        Color.blue(color)
                );
			}
		}
		tree = setQuadTree(0,0,data.length,data[0].length);
		bi = null;
		data = null;
	}
	private void calculateSizeMask(){
		// Initialize size with the max size between height and width
		if(bi.getHeight() <= bi.getWidth())
			size = bi.getWidth();
		else
			size = bi.getHeight();
		
		// Set the size to a pow of 2
		int pow2 = 1;
		while(true){
			if(size==pow2)
				break;
			if(size<pow2){
				size = pow2;
				break;
			}
			pow2 *= 2;
		}
	}
	private Tree setQuadTree(int x, int y, int width, int height){
		if(width==1 && height==1){
			if(data[x][y] == null)
				return new Leaf(EnumQuadTreeCollisionMask.NO_CONTENT);
			if(data[x][y].getAlpha()<=alpha)
				return new Leaf(EnumQuadTreeCollisionMask.NO_CONTENT);
			return new Leaf(EnumQuadTreeCollisionMask.CONTENT);
		}
		else{
			Tree t1 = setQuadTree(x, y, width/2, height/2);
			Tree t2 = setQuadTree(x+height/2, y, width/2, height/2);
			Tree t3 = setQuadTree(x, y+width/2, width/2, height/2);
			Tree t4 = setQuadTree(x+width/2, y+height/2,width/2, height/2);
			if((t1 instanceof Leaf)
				&&(t2 instanceof Leaf)
				&&(t3 instanceof Leaf)
				&&(t4 instanceof Leaf)){
				if(((Leaf)t1).content.equals(((Leaf)t2).content)
					&&((Leaf)t1).content.equals(((Leaf)t3).content)
					&&((Leaf)t1).content.equals(((Leaf)t4).content)){
					return t1;
				}
			}
			return new QuadTree(t1,t2,t3,t4);
		}
	}
    @Override
    public boolean isCollision(FixedRepresentation r1, GameCoordinates gc1, FixedRepresentation r2, GameCoordinates gc2){
        if(r1.getCollisionMask() instanceof CollisionMaskQuadTree &&
                r2.getCollisionMask() instanceof CollisionMaskQuadTree){
            CollisionMaskQuadTree mask1 = (CollisionMaskQuadTree) r1.getCollisionMask();
            CollisionMaskQuadTree mask2 = (CollisionMaskQuadTree) r2.getCollisionMask();
            return isCollision(mask1.getTree(),
                    gc1.getPoint("x").intValue(),
                    gc1.getPoint("y").intValue(),
                    ((Representation2D)r1).getRepresentation().getWidth(),
                    ((Representation2D)r1).getRepresentation().getHeight(),
                    mask2.getTree(),
                    gc2.getPoint("x").intValue(),
                    gc2.getPoint("y").intValue(),
                    ((Representation2D)r2).getRepresentation().getWidth(),
                    ((Representation2D)r2).getRepresentation().getHeight());
        }
        return false;
    }/**
     * Calculate a collision for two quad tree.<br/>
     * Return true if there is a collision, else false
     * @param t1 quad tree 1
     * @param x1 X position to quad tree 1
     * @param y1 Y position to quad tree 1
     * @param width1 width to quad tree 1
     * @param height1 height to quad tree 1
     * @param t2 quad tree 2
     * @param x2 X position to quad tree 2
     * @param y2 Y position to quad tree 2
     * @param width2 width to quad tree 2
     * @param height2 height to quad tree 2
     * @return boolean
     */
    private static boolean isCollision(
            Tree t1, int x1, int y1, int width1, int height1,
            Tree t2, int x2, int y2, int width2, int height2){
        // Get the box of collision
        int xStart = Math.max(x1, x2);
        int xEnd = Math.min(x1+width1, x2+width2);
        if(xEnd <= xStart)
            return false;
        int yStart = Math.max(y1, y2);
        int yEnd = Math.min(y1+height1, y2+height2);
        if(yEnd <= yStart)
            return false;

        // Search a collision
        if(t1 instanceof Leaf && t2 instanceof Leaf){
            if( ((Leaf)t1).content == EnumQuadTreeCollisionMask.CONTENT &&
                    ((Leaf)t2).content == EnumQuadTreeCollisionMask.CONTENT)
                return true;
            return false;
        }else if(t1 instanceof Leaf){
            QuadTree qt = (QuadTree)t2;
            if(	isCollision(t1, x1, y1, width1, height1,
                    qt.nodeWestNorth, x2, y2, width2/2, height2/2) == true)
                return true;
            if(	isCollision(t1, x1, y1, width1, height1,
                    qt.nodeEastNorth, x2+width2/2, y2, width2/2, height2/2) == true)
                return true;
            if(	isCollision(t1, x1, y1, width1, height1,
                    qt.nodeWestSouth, x2, y2+height2/2, width2/2, height2/2) == true)
                return true;
            return isCollision(t1, x1, y1, width1, height1,
                    qt.nodeEastSouth, x2+width2/2, y2+height2/2, width2/2, height2/2);
        }else{
            QuadTree qt = (QuadTree)t1;
            if(	isCollision(qt.nodeWestNorth, x1, y1, width1/2, height1/2,
                    t2, x2, y2, width2, height2) == true)
                return true;
            if(	isCollision(qt.nodeEastNorth, x1+width1/2, y1, width1/2, height1/2,
                    t2, x2, y2, width2, height2) == true)
                return true;
            if(	isCollision(qt.nodeWestSouth, x1, y1+height1/2, width1/2, height1/2,
                    t2, x2, y2, width2, height2) == true)
                return true;
            return isCollision(qt.nodeEastSouth, x1+width1/2, y1+height1/2, width1/2, height1/2,
                    t2, x2, y2, width2, height2);
        }
    }
}
