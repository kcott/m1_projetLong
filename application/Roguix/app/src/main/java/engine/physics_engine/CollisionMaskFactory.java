package engine.physics_engine;

import java.util.List;

import engine.CriteriaEngine;

/**
 * Created by cottin on 22/05/15.
 */
public abstract class CollisionMaskFactory {
    public static CollisionMask2D newCollisionMask(List<CriteriaEngine> criterias){
        if(criterias.contains(CriteriaEngine.TWO_D)){
            return new CollisionMaskQuadTree();
        }
        return null;
    }
}
