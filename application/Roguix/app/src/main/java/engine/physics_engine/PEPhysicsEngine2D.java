package engine.physics_engine;

import engine.EngineCoordinates;
import game.rule_game.Element;

/**
 * Created by cottin on 20/05/15.
 */
class PEPhysicsEngine2D implements PhysicsEngine, PhysicsEngine2D{
    public boolean isCollision(
            CollisionMask2D mask1,
            EngineCoordinates c1,
            CollisionMask2D mask2,
            EngineCoordinates c2){
        return false;
    }
    public void move(Element e, EngineCoordinates c){

    }
}
