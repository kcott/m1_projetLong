package engine.physics_engine;

class QuadTree extends Tree{
	public Tree nodeWestNorth;
	public Tree nodeEastNorth;
	public Tree nodeWestSouth;
	public Tree nodeEastSouth;
	
	/**
	 * Create an empty quad tree
	 */
	public QuadTree(){}
	/**
	 * Create a no empty quad tree.
	 * @param nodeWestNorth first subtree
	 * @param nodeEastNorth second subtree
	 * @param nodeWestSouth third subtree
	 * @param nodeEastSouth fourth subtree
	 */
	public QuadTree(Tree nodeWestNorth,
					Tree nodeEastNorth,
					Tree nodeWestSouth,
					Tree nodeEastSouth){
		this.nodeWestNorth = nodeWestNorth;
		this.nodeEastNorth = nodeEastNorth;
		this.nodeWestSouth = nodeWestSouth;
		this.nodeEastSouth = nodeEastSouth;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "{"
				+nodeWestNorth+","
				+nodeEastNorth+","
				+nodeWestSouth+","
				+nodeEastSouth
				+"}";
	}
}
