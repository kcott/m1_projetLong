package engine.physics_engine;

class Leaf extends Tree{
	/**
	 * Content of the leaf.
	 */
	public Object content;
	/**
	 * Create a empty leaf.
	 */
	public Leaf(){
		
	}
	/**
	 * Create a leaf with content.
	 * @param o
	 */
	public Leaf(Object o){
		content = o;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return content.toString();
	}
}
