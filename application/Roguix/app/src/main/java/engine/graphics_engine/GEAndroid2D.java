package engine.graphics_engine;

import android.graphics.Canvas;
import android.util.Log;

import engine.multimedia.Representation2D;
import game.rule_game.Element;
import game.rule_game.Mapping;

/**
 * Created by cottin on 20/05/15.
 */
class GEAndroid2D implements GraphicsEngine, GraphicsEngine2D {
    @Override
    public void drawElement(Object container, Element element, String key, int shiftX, int shiftY){
        if(container instanceof Canvas){
            Canvas canvas = (Canvas) container;
            canvas.drawBitmap(
                    ((Representation2D) element.getRepresentation(key)).getRepresentation(),
                    element.getCoordinates().getPoint("x").intValue() - shiftX,
                    element.getCoordinates().getPoint("y").intValue() - shiftY,
                    null);
        }else{
            Log.d("GEAndroid2D.drawElement()", "container is not a Canvas");
        }
    }
    @Override
    public void drawMap(
            Object container,
            Mapping mapping,
            int xStart,
            int yStart,
            int xEnd,
            int yEnd,
            int shiftX,
            int shiftY){
        if(container instanceof Canvas){
            Canvas canvas = (Canvas) container;
            for(int xx=Math.max(0,xStart); xx<Math.min(xEnd,mapping.getWidth()); xx++)
                for(int yy=Math.max(0,yStart); yy<Math.min(yEnd,mapping.getHeight()); yy++)
                    canvas.drawBitmap(
                            ((Representation2D) mapping.getTile(xx,yy).getRepresentation("tile")).getRepresentation(),
                            mapping.getTile(xx,yy).getCoordinates().getPoint("x").intValue() - shiftX,
                            mapping.getTile(xx,yy).getCoordinates().getPoint("y").intValue() - shiftY,
                            null);
        }else{
            Log.d("GEAndroid2D.drawMap()", "container is not a Canvas");
        }
    }
}
