package engine.graphics_engine;

import game.rule_game.Element;
import game.rule_game.Mapping;

/**
 * Created by cottin on 11/05/15.
 */
public interface GraphicsEngine2D {
    public void drawElement(
            Object container,
            Element element,
            String key,
            int shiftX,
            int shiftY);
    public void drawMap(
            Object container,
            Mapping mapping,
            int xStart,
            int yStart,
            int xEnd,
            int yEnd,
            int shiftX,
            int shiftY);
}
