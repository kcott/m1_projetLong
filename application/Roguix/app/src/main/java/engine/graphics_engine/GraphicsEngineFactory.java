package engine.graphics_engine;

import java.util.List;

import engine.CriteriaEngine;

/**
 * Created by cottin on 22/05/15.
 */
public abstract class GraphicsEngineFactory {
    public static GraphicsEngine newGraphicsEngine(List<CriteriaEngine> criterias){
        if(criterias.contains(CriteriaEngine.TWO_D)){
            return new GEAndroid2D();
        }
        return null;
    }
}
