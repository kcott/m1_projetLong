package engine;

import java.util.List;

import engine.graphics_engine.GraphicsEngine;
import engine.graphics_engine.GraphicsEngineFactory;
import engine.physics_engine.PhysicsEngine;
import engine.physics_engine.PhysicsEngineFactory;

/**
 * Created by cottin on 11/05/15.
 */
public class Engine {
    public GraphicsEngine graphicsEngine;
    public PhysicsEngine physicsEngine;
    public Engine(List<CriteriaEngine> criterias){
        if(criterias.contains(CriteriaEngine.TWO_D)){
            graphicsEngine = GraphicsEngineFactory.newGraphicsEngine(criterias);
            physicsEngine = PhysicsEngineFactory.newPhysicsEngine(criterias);
        }
    }
}
