package engine;

/**
 * Created by cottin on 11/05/15.
 */
public enum CriteriaEngine {
    TWO_D,
    TWO_POINT_FIVE_D,
    THREE_D,
    FAST,
    LOW_MEMORY
}
