package engine.multimedia;

import android.media.MediaPlayer;

/**
 * Created by cottin on 11/05/15.
 */
public interface Representation {
    public void addSound(String key, MediaPlayer sound);
    public MediaPlayer getSound(String key);
}
