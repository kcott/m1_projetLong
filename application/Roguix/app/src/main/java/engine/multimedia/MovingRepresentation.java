package engine.multimedia;

import android.graphics.Bitmap;

/**
 * Created by cottin on 28/05/15.
 */
public interface MovingRepresentation {
    public void setRepresentation(
            Bitmap bitmap,
            int nRepresentations,
            int widthToARepresentation,
            int heightToARepresentation);
    public FixedRepresentation getFixedRepresentation(int key);
    public void setFixedRepresentation(int key);
    public int getNumberFixedRepresentation();
    public void playAnimation();
    public void stopAnimation();
    public boolean isPlayingAnimation();
}
