package engine.multimedia;

import java.util.List;

import engine.CriteriaEngine;

/**
 * Created by cottin on 22/05/15.
 */
public abstract class RepresentationFactory {
    public static Representation newPhysicsEngine(List<CriteriaEngine> criterias){
        if(criterias.contains(CriteriaEngine.TWO_D)){
            return new MRAnimation();
        }
        return null;
    }
}
