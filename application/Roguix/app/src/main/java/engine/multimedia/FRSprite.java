package engine.multimedia;

import android.graphics.Bitmap;
import android.media.MediaPlayer;

import engine.physics_engine.CollisionMask2D;

/**
 * Created by cottin on 20/05/15.
 */
class FRSprite implements Representation, Representation2D, FixedRepresentation{
    private Bitmap image;
    private CollisionMask2D mask;

    public FRSprite() {

    }
    @Override
    public void setCollisionMask(CollisionMask2D mask){
        this.mask = mask;
    }
    @Override
    public CollisionMask2D getCollisionMask(){
        return mask;
    }
    @Override
    public Bitmap getRepresentation(){
        return image;
    }
    @Override
    public void setRepresentation(Bitmap image){
        this.image = image;
    }
    @Override
    public void addSound(String key, MediaPlayer sound){

    }
    @Override
    public MediaPlayer getSound(String key){
        return null;
    }
}
