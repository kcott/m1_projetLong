package engine.multimedia;

import android.graphics.Bitmap;
import android.graphics.BitmapRegionDecoder;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by cottin on 20/05/15.
 */
class MRAnimation implements Representation, Representation2D, MovingRepresentation{
    private List<FRSprite> sprites;
    private Map<String,MediaPlayer> sounds;
    private int currentSprite;
    private boolean isPlaying;

    public MRAnimation(){
        sprites = new LinkedList<FRSprite>();
        sounds = new HashMap<String,MediaPlayer>();
    }
    @Override
    public Bitmap getRepresentation(){
        return sprites.get(currentSprite).getRepresentation();
    }
    @Override
    public void setRepresentation(
            Bitmap bitmap,
            int nRepresentations,
            int widthToARepresentation,
            int heightToARepresentation){
        for(int n=0; n<nRepresentations; n++){
            BitmapRegionDecoder decoder = null;
            try {
                // Transform Bitmap to BitmapRegionDecoder
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                InputStream inputStream = new ByteArrayInputStream(stream.toByteArray());
                decoder = BitmapRegionDecoder.newInstance(inputStream, false);

                // Cut region to have an animation array
                int nWidth = decoder.getWidth() / widthToARepresentation;
                int nHeight = decoder.getHeight() / heightToARepresentation;

                int left   = (n % nWidth) * widthToARepresentation;
                int top    = (n % nHeight) * heightToARepresentation;
                int right  = left + widthToARepresentation;
                int bottom = top + heightToARepresentation;

                FRSprite sprite = new FRSprite();
                sprite.setRepresentation(
                        decoder.decodeRegion(
                                new Rect(left, top, right, bottom),
                        null));
                sprites.add(sprite);
            }catch (Exception e){
                Log.d("MRAnimations.loadAnimation()", e.toString());
            }
        }
    }
    @Override
    public void playAnimation(){
        isPlaying = true;
    }
    @Override
    public void stopAnimation(){
        isPlaying = false;
    }
    @Override
    public boolean isPlayingAnimation(){
        return isPlaying;
    }
    @Override
    public MediaPlayer getSound(String key){
        return sounds.get("key");
    }
    @Override
    public void addSound(String key, MediaPlayer sound){
        sounds.put(key,sound);
    }
    @Override
    public FixedRepresentation getFixedRepresentation(int key){
        return sprites.get(currentSprite);
    }
    @Override
    public void setFixedRepresentation(int key){
        currentSprite = key;
    }
    public int getNumberFixedRepresentation(){
        return sprites.size();
    }
}
