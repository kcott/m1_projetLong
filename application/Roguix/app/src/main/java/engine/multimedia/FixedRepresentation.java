package engine.multimedia;

import android.graphics.Bitmap;

import engine.physics_engine.CollisionMask2D;

/**
 * Created by cottin on 28/05/15.
 */
public interface FixedRepresentation {
    public void setRepresentation(Bitmap bitmap);
    public CollisionMask2D getCollisionMask();
    public void setCollisionMask(CollisionMask2D mask);
    public Bitmap getRepresentation();
}
