package engine.multimedia;

import android.graphics.Bitmap;

/**
 * Created by cottin on 20/05/15.
 */
public interface Representation2D {
    public Bitmap getRepresentation();
}
